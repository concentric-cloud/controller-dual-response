## Dual Response for Symfony Controllers

This bundle makes it easy to build an API at the same time as you build the UI for your Symfony application using Twig. Inject this service in your controller, and use it to return the content of your page. If the request contains a `Content-Type: application/json` header, the array you pass to the function will be returned as JSON. All other content type headers will return the HTML page from Twig.

This package doesn't handle anything like authentication or routing, you need to do this in your controller. 

# Install instructions

Run on the cli:

    composer require concentriccloud/dual-response-bundle

Then add the following to your services.yaml file:

    ConcentricCloud\DualResponseBundle\:
        resource: '../vendor/concentriccloud/dual-response-bundle/src/'
        autowire: true;


# Using in your Controller

    <?php
    // src/Controller/DemoController.php

    namespace App\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use ConcentricCloud\DualResponseBundle\DependencyInjection\ControllerDualResponseExtension;

    class DemoController extends AbstractController
    {
    
        #[Route('/demo', name: 'demo_controller')]
        public function demo_controller(Request $request, ControllerDualResponseExtension $dResponse): Response
        {

            $returnData = array(
                'foo' => 'bar'
            );

            // Be sure to pass through the request data from the controller

            return $dResponse->response($request, $returnData, 'demo/index.html.twig');

        }

    }