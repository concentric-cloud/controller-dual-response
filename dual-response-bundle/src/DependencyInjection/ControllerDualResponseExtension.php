<?php

namespace ConcentricCloud\DualResponseBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension; 

class ControllerDualResponseExtension extends Extension
{

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function load(array $configs, ContainerBuilder $container)
    {
    }

    public function response(Request $httpRequest, $returnData, $twigTemplateFile) {

        $request_content_type = $httpRequest->headers->get("Content-Type");

        if($request_content_type == "application/json") {

            $response = new Response();
            $response->setContent(json_encode($returnData));
            $response->headers->set('Content-Type', 'application/json');

            return $response;

        }
        else {

            $response = new Response($this->twig->render($twigTemplateFile, $returnData));

        }

        return $response;

    }

}